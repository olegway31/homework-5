const gameField0 = [
  [null, null, null],
  [null, null, null],
  [null, null, null],
];
// Массив где выигрывают X
const gameField1 = [
  ['x', 'o', null],
  ['x', null, 'o'],
  ['x', 'o', 'x'],
];
const gameField2 = [
  ['x', 'o', null],
  ['o', 'x', 'o'],
  [null, null, 'x'],
];
const gameField3 = [
  [null, null, 'x'],
  ['o', 'o', 'x'],
  [null, 'o', 'x'],
];
// Массив где выигрывают O
const gameField4 = [
  ['o', 'x', 'x'],
  ['o', null, 'x'],
  ['o', 'x', 'o'],
];
const gameField5 = [
  ['o', 'x', 'x'],
  ['o', null, 'x'],
  ['o', 'x', 'o'],
];

const gameField6 = [
  ['x', 'x', 'o'],
  [null, 'o', null],
  ['o', null, 'x'],
];

// Массив где ничья
const gameField7 = [
  ['x', 'o', 'x'],
  ['o', 'x', 'x'],
  ['o', 'x', 'o'],
];
const gameField8 = [
  ['o', 'o', 'x'],
  ['x', 'x', 'o'],
  ['o', 'x', 'x'],
];
const gameField9 = [
  ['x', 'x', 'o'],
  ['o', 'x', 'x'],
  ['x', 'o', 'o'],
];
const allGameFields = [
  gameField1,
  gameField2,
  gameField3,
  gameField4,
  gameField5,
  gameField6,
  gameField7,
  gameField8,
  gameField9,
];

const winnerCombinations = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

// ======================= MAIN =======================
// const gameField = getGameField();
console.log('result = ', calculateWinners(gameField0));
// ====================================================

// Получить случайное число от 0 до N
function getRandomValue(gameFields) {
  const min = 0;
  const max = gameFields.length - 1;
  return Math.floor(Math.random() * (max - min + 1) + min);
}

// Получить игровое поле
function getGameField(value = getRandomValue(allGameFields)) {
  console.log(value);
  return allGameFields[value];
}

// Проверка есть ли Нулевое значение. Если есть то игра незакончена
function hasNullValue(squares) {
  for (let cell of squares) {
    if (cell === null) return true;
  }
  return false;
}

// Вычислить победителя в игре
function calculateWinners(currentGameField, lines = winnerCombinations) {
  const squares = [...currentGameField.flat()];

  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return 'Победили -> ' + squares[a];
    }
  }

  if (!hasNullValue(squares)) {
    return 'Ничья';
  }

  return null;
}
